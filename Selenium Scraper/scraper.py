#INSTALLS
#pip install selenium

# IMPORTS
from asyncio import wait
from telnetlib import EC
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
browser=webdriver.Chrome(executable_path=r"C:\chromedriver_win32\chromedriver.exe")


# base url
browser.get('https://xcglobe.com/flights')

# get table in html page by class name 
flight_table  = browser.find_element(By.CLASS_NAME, 'tbl-flights')  # Find the search box

# inside table find all table rows
rows = flight_table.find_elements(By.TAG_NAME, "tr") # get all of the rows in the table
counter = 1

# web page uses pagination, iterate through 2 pages
page = 0
while page < 2:

    # on each page iterate through rows and extract column information
    for row in rows:
        col = row.find_elements(By.TAG_NAME, "td")
        if (len(col) != 0):
            pilot = row.find_elements(By.TAG_NAME, "td")[3] 
            kilometers = row.find_elements(By.TAG_NAME, "td")[4] 
            model = row.find_elements(By.TAG_NAME, "td")[6] 
            team = row.find_elements(By.TAG_NAME, "td")[7] 

            print(str(counter) + ' ' + pilot.text + ' ' + kilometers.text + ' ' + model.text + ' ' + team.text)
            counter = counter + 1

    
    # after first page is scraped move to another page 
    # first scroll down the page so back date button is vissible and then click button
    browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    nextDateButton = browser.find_element(By.XPATH, '//*[@id="back-date"]/button').click()
    time.sleep(0.5)

    # increase page number
    page = page + 1
    
# close chrome driver
browser.close()

